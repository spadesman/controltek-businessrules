


select * from oe_line_po
where po_no = 4038888

select top 1 * from po_line


begin transaction

declare @expediteDate datetime = '2018-01-08'
, @ponumber int = 4038888
, @poLineNumber int = 1

--update ol
--set ol.expedite_date = @expediteDate
--from oe_line ol
--where ol.oe_line_uid = @lineUId

select pl.required_date as po_date
, ol.expedite_date from po_hdr ph
inner join po_line pl on ph.po_no = pl.po_no
inner join oe_line_po olp on ph.po_no = olp.po_no
and pl.line_no = olp.po_line_number
inner join oe_line ol on olp.line_number = ol.line_no
and olp.order_number = ol.order_no
where ph.po_no = @ponumber

rollback transaction


declare @expediteDate datetime = '2018-01-08'
, @ponumber int = 4038888
, @poLineNumber int = 1

select order_number as OrderNumber
, line_number as LineNumber
, expedite_date as DateExpedited
, ol.oe_line_uid as LineUid
, pl.po_line_uid as POLineUid
, pl.required_date as PODateRequired
from oe_line_po olp
inner join oe_line ol on olp.order_number = ol.order_no 
and olp.line_number = ol.line_no
inner join po_line pl on olp.po_no = pl.po_no
and olp.po_line_number = pl.line_no
where olp.po_no = @ponumber

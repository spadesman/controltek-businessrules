﻿using System;
using System.Data;
using System.Linq;
using Dapper;
using P21.Extensions.BusinessRule;
using Rule = P21.Extensions.BusinessRule.Rule;

namespace UpdateSalesOrderExpediteDate
{
    public class UpdateSalesOrderExpediteDate:Rule
    {
        public override string GetName()
        {
            return "UpdateSalesOrderExpediteDate";
        }

        public override string GetDescription()
        {
            return "Updates the sales order line when the linked po line is updated";
        }

        public override RuleResult Execute()
        {
            var result = new RuleResult() {Success = true};

            try
            {
                var salesOrderNumber = Data.Set.Tables["d_po_purchase_order_sheet"].Rows[0].Field<string>("sales_order_number");

                if (string.IsNullOrEmpty(salesOrderNumber))
                {
                    return result;
                }

                var poNumber = Data.Set.Tables["d_po_purchase_order_sheet"].Rows[0].Field<decimal>("po_no");

                //Get linked sales order lines
                var sql = @"select order_number as OrderNumber
                        , line_number as LineNumber
                        , expedite_date as DateExpedited
                        , ol.oe_line_uid as LineUid
                        , pl.po_line_uid as POLineUid
                        , pl.expected_ship_date as PODateExpectedShip
                        from oe_line_po olp
                        inner join oe_line ol on olp.order_number = ol.order_no 
                        and olp.line_number = ol.line_no
                        inner join po_line pl on olp.po_no = pl.po_no
                        and olp.po_line_number = pl.line_no
                        where olp.po_no = @ponumber";

                var linkedSalesOrderLines = P21SqlConnection.Query<LinkedSalesOrder>(sql, new {poNumber}).ToList();

                DataTable linesTable = Data.Set.Tables["d_po_item_sheet"];

                foreach (DataRow row in linesTable.Rows)
                {
                    var expectedShipDate = row.Field<DateTime?>("expected_ship_date");

                    //It seems that this value can be null, so we test for it and
                    if (!expectedShipDate.HasValue)
                    {
                        continue;
                    }
                    
                    var poLineUid = row.Field<int>("po_line_uid");

                    //Get the corresponding line from the linked sales orders
                    var linkedSalesOrderLine = linkedSalesOrderLines.FirstOrDefault(x => x.POLineUid == poLineUid);

                    //If a match is found move on to the next line
                    if (linkedSalesOrderLine == null)
                    {
                        continue;
                    }

                    //Compare the date from the db with the date from the form
                    if (linkedSalesOrderLine.PODateExpectedShip.Date == expectedShipDate.Value.Date) continue;
                    
                    //Update the sales order line
                    var updateSql = @"update ol
                                            set ol.expedite_date = @expectedShipDate
                                            from oe_line ol
                                            where ol.oe_line_uid = @lineUId";

                    P21SqlConnection.Execute(updateSql, new {linkedSalesOrderLine.LineUid, expectedShipDate});
                }
            }
            catch (Exception e)
            {
                result.Message = $"Something went wrong. Error message: {e.Message} - {e.StackTrace}";
            }

            return result;
        }
    }

    internal class LinkedSalesOrder
    {
        public int OrderNumber { get; set; }
        public int LineNumber { get; set; }
        public DateTime DateExpedited { get; set; }
        public int LineUid { get; set; }
        public int POLineUid { get; set; }  
        public DateTime PODateExpectedShip { get; set; }    

    }
}

﻿using System;
using P21.Extensions.BusinessRule;
using System.Data;
using Rule = P21.Extensions.BusinessRule.Rule;

namespace NonStockItemDispositionWarning
{
    public class NonStockItemDispositionWarning:Rule
    {
        public override string GetName()
        {
            return "NonStockItemDispositionWarning";
        }

        public override string GetDescription()
        {
            return "Warning for non-stock items";
        }

        public override RuleResult Execute()
        {
            var result = new RuleResult() {Success = true};

            try
            {
                var stockable = Data.Fields["stockable"].FieldValue;
                var disposition = Data.Fields["disposition"].FieldValue;

                if (stockable == "N" && disposition == "B")
                {
                    result.Message = "This item is a non-stock item. Change disposition to D or S before saving!";
                }

            }
            catch (Exception e)
            {
                result.Message = $"Something went wrong with the {GetName()} business rule. Error message: {e.Message} ";
            }

            return result;

        }
    }
}

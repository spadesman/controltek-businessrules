﻿using System;
using P21.Extensions.BusinessRule;

namespace CustomerPartNumberLengthLimit
{
    public class CustomerPartNumberLengthLimit:Rule
    {
        public override string GetName()
        {
            return "CustomerPartNumberLengthLimit";
        }

        public override string GetDescription()
        {
            return "Limits the customer part number to 20 characters";
        }

        public override RuleResult Execute()
        {
            var result = new RuleResult() {Success = true};

            try
            {
                var customerPartNumber = Data.Fields["their_item_id"].FieldValue;

                if (customerPartNumber.Length > 20)
                {
                    result.Success = false;
                    result.Message = "Customer Part Number can be a maximum of 20 characters.  Record will not save if over 20 characters.";
                    Data.SetFocus("their_item_id");
                }

            }
            catch (Exception e)
            {
                result.Message = $"Error Message: {e.Message}";
            }

            return result;
        }
    }
}

﻿using System;
using System.Data;
using P21.Extensions.BusinessRule;
using Rule = P21.Extensions.BusinessRule.Rule;

namespace AutoCheckCCEmail
{
    public class AutoCheckCcEmail:Rule
    {
        public override string GetName()
        {
            return "AutoCheckCCEmail";
        }

        public override string GetDescription()
        {
            return "Auto checks the cc box when emailing customers";
        }

        public override RuleResult Execute()
        {
            var result = new RuleResult() {Success = true};

            try
            {
                var rows = Data.Set.Tables["d_dw_email_recipients"].Rows;

                foreach (DataRow row in rows)
                {
                   row.SetField("cc_sender", "Y");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return result;
        }
    }
}

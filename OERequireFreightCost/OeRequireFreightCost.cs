﻿using System;
using System.Data;
using P21.Extensions.BusinessRule;
using Rule = P21.Extensions.BusinessRule.Rule;

namespace OERequireFreightCost
{
    public class OeRequireFreightCost:Rule
    {
        public override string GetName()
        {
            return "OeRequireFreightCost";
        }

        public override string GetDescription()
        {
            return "Require Freight cost for FRT item id";
        }

        public override RuleResult Execute()
        {
            var result = new RuleResult() {Success = false};
            var freightMissingCosts = false;
            try
            {
                var rmaFlag = Data.Set.Tables["d_oe_header"].Rows[0].Field<string>("rma_flag") == "Y";

                if (rmaFlag)
                {
                    result.Success = true;
                    return result;
                }

                DataTable linesTable = Data.Set.Tables["d_dw_oe_line_dataentry"];

                foreach (DataRow row in linesTable.Rows)
                {
                    var itemId = row.Field<string>("oe_order_item_id");

                    if (itemId != "FRT") continue;
                    
                    //Check to see if the other cost has a non-null value
                    var orderCost = row.Field<decimal>("sales_cost");
                    var commissionCost = row.Field<decimal>("commission_cost");

                    if (orderCost > 0 && commissionCost > 0) continue;
                    freightMissingCosts = true;
                    break;
                }

                if (freightMissingCosts)
                {
                    result.Message = "Please add order and commission cost for FRT (item).  Order will not save without a cost";
                }
                else
                {
                    result.Success = true;

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            return result;
        }
    }
}

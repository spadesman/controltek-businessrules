﻿using System;
using P21.Extensions.BusinessRule;

namespace CreditCardOnlyAccountPrompt
{
    public class CreditCardOnlyAccountPrompt:Rule
    {
        public override string GetName()
        {
            return "CreditCardOnlyAccountPrompt";
        }

        public override string GetDescription()
        {
            return "Prompt for credit card only accounts";
        }

        public override RuleResult Execute()
        {
            var result = new RuleResult() {Success = true};

            try
            {
                var termsId = Data.Fields["oe_hdr_terms"].FieldValue;

                if (termsId == "4" || termsId == "36")
                {
                    result.Message =
                        "This account is a credit card only account.  Please confirm that credit card is on file and has been authorized for the amount of this order before proceeding.";
                }
            }
            catch (Exception e)
            {
                result.Message = $"Error Message: {e.Message}";
            }

            return result;
        }
    }
}

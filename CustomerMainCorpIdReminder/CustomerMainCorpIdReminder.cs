﻿using P21.Extensions.BusinessRule;

namespace CustomerMainCorpIdReminder
{
    public class CustomerMainCorpIdReminder:Rule
    {
        public override string GetName()
        {
            return "CustomerMainCorpIdReminder";
        }

        public override string GetDescription()
        {
            return "Remind user about Corp Address Id on Customer Maintenance screen";
        }

        public override RuleResult Execute()
        {
            var result = new RuleResult
            {
                Success = true,
                Message =
                    "Please confirm that the Corp Address ID is correct.  The Corp Address ID should be the Master ID that all ‘Customers’ roll up under.  For instance, Loomis may have 20 ‘Customer IDs’ however roll up to one Corp Address (Master) ID for reporting purposes."
            };


            return result;
        }
    }
}
